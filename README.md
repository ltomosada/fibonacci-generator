# fibonacci generator

A full stack project allowing users to generate a Fibonacci sequence of their desired length. It is built using Django for both the front and back end, and SQLite as the database. 


## To Run

1. Create a virtual environment by typing `python -m venv .venv` into the terminal
2. Activate virtual environment with:
   - Mac: `source .venv/bin/activate`
   - Windows: `.\.venv\Scripts\Activate.ps1`
3. Upgrade pip using `python -m pip install --upgrade pip`
4. Install requirements using `pip install -r requirements.txt`
5. Migrate using `python manage.py migrate`
6. Start server using `python manage.py runserver`
7. Navigate to localhost:8000 in browser and explore!