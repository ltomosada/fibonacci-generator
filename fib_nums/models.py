from django.db import models


# Create your models here.
class Fibonacci(models.Model):
    input = models.PositiveSmallIntegerField(null=False, unique=True)
    result = models.TextField()
