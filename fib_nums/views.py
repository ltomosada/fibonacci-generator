from django.shortcuts import render, redirect
from fib_nums.models import Fibonacci


def submit_input(request):
    """
    This function runs when a user submits a post request with their desired
    input number. The function stores the input value in the request.session
    object to be passed to the show_fibonacci function later.

    Then it checks to see if that input is already in the database. If it is
    not, it will pass the input value to the calculate_fibonacci function to
    get the sequence and create a new object in the database.

    Then it will redirect the user to the result page.
    """
    if request.method == "POST":
        input_value = int(request.POST.get("input"))
        request.session["input_value"] = input_value
        if not Fibonacci.objects.filter(input=input_value).exists():
            fibonacci_sequence = __calculate_fibonacci(input_value)
            Fibonacci.objects.create(input=input_value, result=fibonacci_sequence)
        return redirect("result_page")
    else:
        return render(request, "form.html")


def __calculate_fibonacci(input_value):
    """"
    This function will run if the entered input does not already exist in the
    database.

    If any Number object exists, it will utilize the current maximum input
    value and build off of that one's result if the input value is greater
    than the maximum or take a slice of the result if it is less than
    the maximum value.

    If no objects exist, the Fibonacci sequence will be calculated using the
    logic in the else statement.

    This will prevent unnecessary recalculations of the sequence.

    """
    if Fibonacci.objects.all().exists():
        max_fib = Fibonacci.objects.all().order_by("-input").first()
        max_input = max_fib.input
        if max_input == 1:
            max_result = "1, 1"
        else:
            max_result = max_fib.result
        res = max_result.split(", ")
        if input_value > max_input:
            while len(res) < input_value:
                res.append(str(int(res[-1]) + int(res[-2])))
            return ", ".join(res)
        else:
            return ", ".join(res[:input_value])

    else:
        if input_value == 1:
            return "1"
        elif input_value == 2:
            return "1, 1"
        else:
            res = ["1", "1"]
            for i in range(2, input_value):
                res.append(str(int(res[-1]) + int(res[-2])))
            return ", ".join(res)


def show_fibonacci(request):
    """
    This function displays the results for the requested input. It retrieves
    the input_value from the submit_input function and uses it as context
    to pass to the rendering of the result.html template on the result page.
    """
    fib = Fibonacci.objects.get(input=request.session["input_value"])
    return render(request, "result.html", {"fibonacci": fib})
