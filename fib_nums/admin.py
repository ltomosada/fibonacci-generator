from django.contrib import admin

# Register your models here.
from fib_nums.models import Fibonacci


class FibonacciAdmin(admin.ModelAdmin):
    pass


admin.site.register(Fibonacci, FibonacciAdmin)
