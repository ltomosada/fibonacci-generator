from django.apps import AppConfig


class FibNumsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "fib_nums"
